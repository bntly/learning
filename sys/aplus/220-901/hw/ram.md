---
title: RAM
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Compare and contrast various RAM types and their features.
    cite: CompTIA A+
resources: [cbtnaplus901,profaplus901,pvaplus]
todos: 
    - Research buffered memory
    - Research ECC and parity
    - Research CAS Latency
---
RAM (Random Access Memory) is used for programs and data as well as by the operating system for disk caching (using RAM to hold recently accessed data). Therefore, installing more RAM improves transfers between the CPU and both RAM and hard drives. The hard disk can be used as virtual memory in the event that your computer is short on RAM, but it is much slower. Don't confuse RAM with hard mass storage devices such as hard disks and SSDs, though the hard drive can be used as a slow substitute for RAM. RAM and hard disk contents can be changed freely; however, RAM loses its contents as soon as the computer shuts down, while magnetic storage can hold data for years. RAM's contents are temporary and RAM is much faster than magnetic or SSD speeds. RAM speeds are measures in nanoseconds (billionths of a second) where mass storage is measured in milliseconds (thousandths of a second). RAM is ever-increasing in terms of need as OS and applications become more powerful and feature rich.

## Types
Although a few systems can use more than one memory module form factor, if you wanto to upgrade to a faster type of memory, such as 184-pin DIMM (DDR SDRAM) to 240-ping DIM (DDR2 or DDR3 SDRAM), you may have to upgrade the motherboard first. Most memory module types use one type of memory, though some older memory module types, such as early 186-pin DIMMs, were available with different types of memory chips. In rder to provide stable performance and avoid conflicts with onboard memory, you must be aware of the right memory.

There are 3 ways to speify the speed of a memory module: the actual speed in ns of the chips on the module (60ns), the clock speed of the data bus (PC800 is 800MHz), or the throughput (in Mbps) of the memory.
> For example, PC3200 in 3200Mbps or 3.2GHzl PC2-2 6400 is 6400Mbps or 6.4Gbps; PC3-12800 is 12800Mbps or 12.8Gbps). Currently the throughput method is used.

Latency is how quickly memory can switch between rows. Modules with the same speed can differ in latency, and all modules in a bank should have the same latency, size, and speed.

Most system do not perform parity checking, which verifies the contents of memory, but some motherboards and systems support these functions. Parity checking can slow down the system, but with ECC memory, errors can be detected and corrected. This is commonly used on critical systems, such as high level mathematics, financial systems, or departmental or enterprise level server tasks. Some systems also support buffered (registered) or nonregistered modules. Buffered modules are slower due to a hip that boosts the memory signal, but are more reliable.

Some motherboards insist using the same speeds and sizes of memory in each socket, and others are more flexible.

Systems address memory in banks and the number of modules per banks varies due to the processor and the memory module type installed., If you need more than one module per bank, and only one module is installed, the system will ignore it. Systems that require multiple modules per bank require that module be the same size and speed.

The number of sockets on the motherboard determines the number of modules that can be installed. Small form factors, such as the microATX and Mini-ITX , often only support one or two modules, but systems that use full size ATX motherboards often support three or more, especially those designed for multi-channel memory, with two or more modules accessed as a single logical unit for faster performance.

Almost all memory modules use some type of dynamic RAM (DRAM) chips. DRAM requires frequent recharges of memory to retain its contents (refreshes).

SRAM, os Static RAM, is RAM that does not need to be periodically refreshed. Memory refreshing is common to other types of RAM and is basically the act of reading information from a specific area in memory and immeidiately rewriting back to the same area without modification. Due to SRAMs architecture it does not require this. SRAM is often used as cache memory for CPUs, as buffers within hard drives, and as temporary storage for LCD screens. SRAM is normally soldered directly to a printer circuit board (PCB) or integrated into a chip. This means that you usually won't be replacing SRAM. SRAM is faster and found in smaller quantities than DRAM.

SDRAM was the first type of memory to run in sync with the processor bus (connection between the processor, or CPU, and other motherboard components). Most 168-pin DIMMs use SDRAM. To determine whether a DIMM contains SDRAM memory, check its speed markings. SDRAM is rated by bus speed:

* PC66 is 66MHz
* PC100 is 100MHz
* PC133 is 133MHz

All SDRAM have a one bit prefetch buffer and perform on transfer per clock cycle. Depending on the module and motherboard chipset combination, PC133 modules can sometimes be used on systems designed for PC100 modules.

Modules of the same type containing chips of the same speed can have different CAS latency (CL). CL refers to how wuickly memory column addresses can be accessed. Lower CL indicates faster access than higher CL. Most module labels indicate the CL values, but not all.

### DDR
*Double Data Rate* SDRAM are run on second generation systems. It performs two transfers per clock cycle and features a 2 bit prefetch buffer. 184-pin DIMM use DDR SDRAM chips.

While often rated in MHz, it is often rated by throughput (Mbps).

Below are the most common speeds for DDR SDRAM, though vendors offer other speeds:

* (200MHz/1600Mbps) - PC1600
* (266MHz/2100Mbps) - PC2100
* (333MHz/2700Mbps) - PC2700
* (400MHz/3200Mbps) - PC3200

### DDR2
The successor to DDR SDRAM, DDR2 runs its external data bus at twice the speed of DDR SDRAM and features a four-bit prefetch buffer, enabling faster performance, though DDR2 has breater latency. Latency is the measure of how long it takes to receive information from memory. Typical latency for DDR2 memory are CL=5 and CL=6, compared wto CL=2.5 and CL=3 for DDR. 240-pin memory modules use DDR2 SDRAM

DDR2 speeds may be referred to by the memory speed of the chips on the moule (memory clock speed x4 or the I/O bus clock speed x2), for example DDR2-533 (133MHz memory clock, or 266MHz I/O bus clock x2, is 533MHz). It may also be the throughput (DDR2-533 is used in PC2-4200 modules which have a throughput of more than 4200Mbps). PC2-\* indicates the module uses DDR2 memory, where PC-\* indicates DDR memory.
Other common speeds for DDR2 SDRAM include PC2-3200 (DDR2-400; 3200Mbps), PC2-5300 (DDR2-667); PC2-6400 (DDR2-800), and PC2-8500 (DDR2-1066)
### DDR3
COmpared to DDR2, DDR3 SDRAM runs at lower voltages, has twice the internal banks, and most version run at faster speeds that DDR2. DDR3 also has an eight-bit prefetch buffer. As with DDR2 to DDR, DDR3 has greater latency than DDR2. Typical latency values for DDR3 are CL7 and CL9. DDR3 modules use 240-pins, but the layout and keyings are different than DDR2, making the not interchangable

Speed for DDR3 follow the same pattern as DDR2 (4x memory clock or 2x I/O bus clock, or throughput). DDR3-1333 (333MHz memory, of 666MHz I/O bus clock, is 1333MHz ). DDR3 uses PC3-10600 modules which have a throughput of 10600Mbps (10.6Gbps). PC3-\* indicates that the module uses DDR3 memory.

Other common speed include PC3-8500 (DDR3-1066; 8500Mbps), PC3-12800 (DDR3-1600), and PC3-17000 (DDR3-2133).

### *DDR4*
*Intel's X99 chipset for the Haswell-E Core i-series processors released in August 2014, DDR4 is the fourth generation of DDR memory. Compared to DDR3, it runs at even lower voltages (1.2V) than either DDR3 or lower voltage DDR3L. DDR4 supports densities up to 16Gb per chip (twice that of DDR3), twice the memory banks, and uses bank groups to speed up burst accesses to memory, but uses the same eight-bit prefetch buffer as DDR3. Data speeds range from 1600Mbps to 3200Mbps, compared to 800Mbps to 2133Mbps. To improve reliability, DDR4 includes built-in support for CRC and parity, rather than requiring the memory controller to support ECC with parity memory as in DDR3 and earlier designs.*

### SODIMM
*Small Outline Dual Inline Memory Module* is a small form factor memory module used in laptop and some small-footprint min-ITX motherboards and systems. They are about 68mm x 32mm.

### DIMM
*Dual Inline Memory Module* is a full-sized memory module used by desktop computers that has different electrical contacts on each side. and have a 64-bit data width. 

### Parity vs. non-parity
There are two methods to protect the reliability of memory. Both depend on the presence of an additional memory chip over the chips required for the data bus of the module. For example, a module that uses 8 chips for data would need a ninth chip for parity or ECC and a module that uses 16 chips for data (2 banks of eight) would have a 17th and 18th chip for parity.

Parity checking has been around since the original IBM PC. It works by adding a parity bit for a byte of data (1 parity bit for each group of 8 bits) accessed to the value of the data and checking that the value is odd (odd parity). A memory problem typically causes the data bit values plus the parity bit value to be an even number, which triggers a parity error and the system halts with a parity error message. Parity checking requires parity-enabled memory and support in the motherboard.

To fix this problem varies with the system. On older systems, you must open the system and push all the memory chips into place, and test the memory thoroughly if you have no spares (using memory testing software) or replace the memory if you have spare ships. If the computer uses memory modules, replace one module at a time, test the memory (or at least run the computer for a while) to determine whether the problem has gone away. If the problem persists, replace the original module, move onto the next, and repeat.

Some systems provide the logical location of the error so you can refer to the system documentation to determine the module or modules to replace.

Parity checking costs more because of the extra chips involved and the additional features required in the motherboard and chipset. It fell out of fashion in the mid-90s. Systems that lack parity checking will freeze when a memory problem occurs and do not display any message onscreen. 

Because parity checking protects you from bad memory by shutting down the computer and can cause data loss, vendors have created a better way to use parity bits to solve memory errors using a method called ECC

### ECC vs. non-ECC
*Error Correction Code* or *Error Correcting Code* (also called Error Detection and Correction, EDAC) has been in use in network servers for a while. It enables the system to correct single-bit errors and notify of larger errors.

Most desktops do not support ECC, though some work stations and most servers do. On these systems ECC may be enabled or disabled through the BIOS or it may be a standard feature. The parity bit in parity memory is used by the ECC feature to determine when the content of memory is corrupt and to fix single-bit issues. Unlike parity, which only warns of memory errors, ECC actually corrects the errors.

ECC is recommended for maximum data safety, although parity and ECC do have a small impact on performace, you do get the benefit of safety. ECC memory modules use the same types of memory chips used by standard modules, but they will use more chips and might have a different internal design to allow for ECC. ECC, like parity, have an extra bit for each group of 8 bits.

### RAM configurations
| RAM Type | Pins (DIMM) | Pins (SODIMM) | Common Type/Speed | Characteristics | 
|----------|-------------|---------------|-------------------|-----------------|
| DDR SDRAM | 184 | 200 | PC3200 = 400MHz/3200Mbps | Double transfers per clock cycle compared to regular SDRAM |
| DDR2 SDRAM | 240 | 200 | DDR2-800 (PC2-6400) = 800MHz/64000Mbps | External data bus speed (I/O bus clock) is 2x faster than DDR SDRAM |
| DDR3 SDRAM | 240 | 204 | DDR3-1333 (PC3-10600) = 1333Mhz/10600Mbps | External data bus speed is 2x faster than DDR2 (4x faster than DDR) |
| \*DDR4 SDRAM | 288 | 260 | DDR4-2400 (PC4-19200) = 2400MHz/19200Mbps | External data bus speed is 2x faster than DDR3 (8x faster than DDR) |
| \*UniDIMM | NONE | 260 | DDR3 or DDR4 | Designed for Intel Skylake (6th gen Core i-series). Memory controller/processor must support DDR3 or DDR4 memory |
| \*MicroDIMM | NONE | 172(DDR);214(DDR2, DDR3) | DDR,DDR2,DDR3 | 54mm x 32mm size for mobile devices. |
| \*MiniDIMM | NONE | 244 | DDR2 | | 

\* Not on the exam, but good to know.

#### Single channel vs. dual channel vs. triple channel
Systems that are designed to access two or more identical modules as a single unit (multi-channel) provide faster performance than systems that access each module as a unit.

Originally all systems that used SDRAM were single channel. Each 64-bit DIM or SODIMM was addressed individually.

Dual-channel, triple channel, and quad-channel memory are accessed in an interleaved manner to improve memory latency. As a result, systems running dual-channel memory offer faster memory performance than systems running single channel memory. Intel introduced triple-channel memory with its Core i7 processor. Quad-channel is avaialble on high-performance Intel desktop and server platform and AMD server platforms. With reduced performace as a side-effect, all multi-channel systems can run with non-identical modules.

Some systems using DDR and most using DDR2 or newer support dual-channel operation. When two identical (same size, speed, and latency) modules are installed in the proper sockets, the memory controller access them in interleaved mode for faster access. Most systems with two pairs of sockets marked in contrasting colors implement dual-channel in the way. Install matching module in th esame color sockets.

Some systems using Intel's LGA 1366 chipset support triple channel addressing. Most of these systems use two sets of 2 sockets. Populate at least one set with identical memory. SOme systems have a fourth socket, but for best performance, the last socket should not be used.

Some systems using Intel's LGA 2011 chipset support quad-channel addressing. Most systems have two sets of four sockets. Populate one or both sets with identical memory.

### Single sided vs. double sided
A single-sided, or single-ranked , module has a single 64 bit wide bank of memory chips. A double-side, or double-ranked, module has two 64-bit banks of memory stacked for higher capacity. Many, but not all, of these modules use both sides of the module for memory. However the use of smaller chips enabled "double-sided" modules to have all of the chips on one side.

Some systems, primarily older systems using DDR2 or older, have different maximum amounts of RAM based on whether single-sided or double-sided modules are used.

### Buffered vs. unbuffered
Most desktop memory modules use unbuffered memory. Many servers and some desktop and workstation computers use a memory module called registered or buffered memory. Buffered memeory modules have a register chip that enables the system to remain stable when large amounts of memory are installed. It slightly slows down memory access. 

They can be built with or without ECC support, though most buffered memory modules are used by servers and include ECC support.

Buffered memory uses a register to store data in case of a bottle neck, so data can be built up and thn handed off for processing. YOu can see this with RDIMM (Buffered) and UDIMM (Unbuffered).

## RAM compatibility
WIth memory, compatibilty is important. THe memory module must fit the motherboard, speed must be compatible, and the storage size/combination must match your system as well.

The labels on memory modules list the manufacturer, module type, size, and speed. Most also list the CAS latency. You can use this information to purchase additional modules of the same size.

YOu can also visit the memory manufacturer's webiste to to determine whether your motherboard supports it. Some memory vendors, such as Crucial.com, offer a browser based utility that checks your system for installed memory and lists recommendations of memory specific to your system. It may also display the memory speed and size. If installing memory in a system that uses single-sided modules (8 or 9 chips), don't install double-sided modules (16 or 18 chips) as additional or replacement RAM unless you verify they will work in the system.