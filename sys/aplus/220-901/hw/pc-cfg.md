---
title: PC configuration
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Given a scenario, select the appropriate components for a custom PC configuration to meet customer specifications or needs.
    cite: CompTIA A+
---

## Graphic/CAD/CAM design workstation
### Multicore processor
### High-end video
### Maximum RAM
## Audio/video editing workstation
### Specialized audio and video card
### Large fast hard drive
### Dual monitors
## Virtualization workstation
### Maximum RAM and CPU cores
## Gaming PC
### Multicore processor
### High-end video/specialized GPU
### High-definition sound card
### High-end cooling
## Home theater PC
### Surround sound audio
### HDMI output
### HTPC compact form factor
### TV tuner
## Standard thick client
### Desktop applications
### Meets recommended requirements for selected OS
## Thin client
### Basic applications
### Meets minimum requirements for selected OS
### Network connectivity
## Home server PC
### Media streaming
### File sharing
### Print sharing
### Gigabit NIC
### RAID array
