---
title: Power supplies
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Install a power supply based on given specifications.
    cite: CompTIA A+
---
 
## Connector types and their voltages
### SATA
### Molex
### 4/8-pin 12v
### PCIe 6/8-pin
### 20-pin
### 24-pin
## Specifications
### Wattage
### Dual rail
### Size
### Number of connectors
### ATX
### MicroATX
### Dual voltage options
