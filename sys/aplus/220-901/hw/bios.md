---
title: BIOS/UEFI
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Given a scenario, configure settings and use BIOS/UEFI tools on a PC.
    cite: CompTIA A+
resources: [cbtnaplus901,profaplus901,pvaplus]
---
Firmware is encoded software in hardware, like ROM, that can be run without additional instruction from an OS. It is the middle ground between hardware and software. They are attached to motherboards, optical drived, video cards, mass storage adapter (RAID, SATA, SATA express host adapters and cards), network adapaters and cards, modems, printers, and other devices. Basically firmware is software in hardware, specifically chips. Its job is to control the device it is connected to.

The System BIOS (Basic Input/Output System), or on more recent computers the UEFI (Unified Extensible Firmware Interface), are firmware instructions that control the computer on boot and are burned into a flash memory chip on the motherboard, commonly EEPROM (Electrically Erasable Programmable ROM), though previously was stored on and run from a bootable floppy disk.

It tests and initializes components of the system including the processor, memory, video card, hard drive, optical and USB drives. It reports any errors during the testing stage, called POST (power-on self-test). In this program, users can change settings in or upgrade the BIOS. Changes to settings in the BIOS are stored in the CMOS chip.

The CMOS (Complementary Metal Oxide Semiconductor) chip is a special RAM chip that stores basic system information when the computer is turned off. This information can include the date and time, memory and CPU settings, hard drive configuration, and boot sequence. Because the changed to the BIOS are stored in the CMOS, they may be referred to as CMOS settings or BIOS settings. The CMOS chip is integrated with a battery to provide continuous power, even when the computer is off, in order to retain its memory. This battery is generally found next to the CMOS chip itself on the motherboard, and is necessary to keep the CMOS memory non-volatile (NVRAM). In modern computers settings may be stored in flash memory instead of CMOS.

Most of the more recent desktops and laptops, as well as all since 2014, use the UEFI, which displays a mouse-driven GUI or text-based BIOS setup menu. All OS X
computers use UEFI. 

The UEFI is based on the Intel standard EFI. Manufacturers will create a BIOS front end specific to their hardware based on the standard and are slowly replacing BIOS with UEFI.

UEFI provides the following advantages over Flash ROM BIOS:

* Support 2.2TB or higher capacity hard drives. They require the GUID Partition Table (GPT) in order to access full capacity. Also supports FAT and removable media
* Optimizations including faster boot times.
* Additional features, better diagnostics, ability to open shell environment for easy flash updates, and ability to create and reuse mulitple BIOS configurations are available by the capacity that larger ROM chips UEFI provide.

UEFI has a pre-boot environment. Though not an OS, it has its own shell, drivers, and applications. It can browse the internet and backup a storage drive. Can run remote diagnostics without an OS.

Most desktops use UEFI with a mouse-driven GUI, where as many laptops use a text-based interface similar to BIOS.

Older OSes talked to the hardware through the BIOS, instead of accessing the hardware directly. Limited hardware support as there are no drivers for modern network, video, or storage devices.


## Firmware upgrades/flash BIOS
Changes to the hardware (replacing a processor) or software may cause the firmware to become outdated, which can lead to system or device failures, or possibly data loss. Sometimes upgrades are available to resolve bugs or add features. Most firmware can be "flashed", meaning that the contents of the firmware can be changed through software. Upgrading the BIOS is not a normal part of the maintenance process, and should only be done when necessary. Before you do anything, check the firmware version (msinfo32) and confirm that an upgrade is available. Read the documentation to check for a list of new features or if the OS prerequisites are different.

Older BIOS upgrades require a boot disk, such as floppy or digital media. Most now run as an executable, so close out of all programs before using. Most upgrades will check for prerequisites, but isn't always, so double check. Save documents as it will require a reboot.

You can also update the BIOS to solve problems with hardwarew related issues, such as problems with power management.

In prior years, you'd have to reprogram the BIOS chip with an EEPROM burner. If anything went wrong, it could be destroyed.

Although, we now update the BIOS with software, an incomplete or incorrect BIOS update will prevent access to the system. To prevent issues, follow these steps:

  1. Backup important data
  2. Record current BIOS configurationm especially hard drive settings. (These settings may need to be re-entered)
  3. Find a reliable power source. Use laptop with full battery. With a desktop, use an UPS if possible.

**Updating BIOS**

  1. Navigate to the PC manufacturer website and lok for downloads or support links. Avoiding beta releases, find the BIOS update for your system model or motherboard.
  2. Determine the installation media needed to install the BIOS. Recent systems use a Windows-based installed, but some use a bootable CD or USB drive.
  3. Download all necessary files. Most contains the image and loader, though some might require a separate loader program.
  4. If using bootable media, make sure the drive is the first item in the BIOS boot sequence, insert the media, and restart the system. If prompted, press a key to start the upgrade process. Some upgrades run themselves and some require you to choose an image from a menu. Some even require teh actual filename of the BIOS. If prompted to save the current BIOS image, do so.
  5. DO NOT TURN OFF POWER TO THE SYSTEM WHEN THE BIOS UPDATE IS RUNNING. If power is cut during the update, then the chip could become useless.
  6. Remove the media and restart the system.
  7. Reconfigure the BIOS settings if needed.



Some motherboard vendors offer dual BIOS chips on some products. THe secondary BIOS performs the same functions as the primary, so you can switch between them. You can even upgrade via USB, without even powering on the system.

**Recovery when a BIOS update fails**

If you use the wrong update to update the BIOS, or if the process did not finish, the system cannot start. You may need to contact the vendor to purchase a new BIOS chip. Some systems have a mini-BIOS that can be reinstalled from a reserved portion of the chip. Systems with this feature have a jumper on the motherboard called the Flash recovery jumper. To use this feature you would first download the correct BIOS update and make a bootable disc. Next, you would set the jumper to recovery , insert the disc and re-run the setup process. Video will not work, so you will have to listen for beeps and the drive light to run during the process. Turn off the comuter, reset the jumper to normal, and restart the computer. Your system may have a sumper that write protects the BIOS. This may prevent the BIOS from recovery or update. You would simply disable the write protection, perform the update, and re-enable write protection.

## BIOS component information
Most systems display system information such as the processor type, clock speed, cache memory size, installed memory (RAM), and other BIOS information from within the BIOS itself. You may need to review multiple screens to find all the information.

### RAM
Provides information on the system memory and the extended memory.

### Hard drive
Provides information on the hard drive and its capabilities.

### Optical drive
Provides information on the optical drive.

### CPU
Provides information on the CPU.

## BIOS configurations
There are many options to select when configuring the BIOS settings. Some BIOS firmware allows you to automatically configure the BIOS based on a couple of defaults that deal with performance such as memory timings and cache, and are generally customized by the manufacturer of the system or motherboard. BIOS defaults, or Original/Fail-Safe on some systems, can be used to troubleshoot the system as the settings are conservative in memory timings and the like. Typically the setup, or optimal, defaults provide better performance and using these defaults the system will work acceptably.

Be aware that any changes to the settings prior to using a default will be overridden.

Generally, you increment numeric settings, such as date and time, with the `+`/`-` or page up/page down keys. When working with settings with limited options, such as enable disable, you'll likely press the `Enter` or the right arrow key to navigate into the options and make the desired selection.

### Boot sequence
Provides ability to configure the order of devices in which the system checks for bootable media. You can use this to configure the system to boot from the OS, bootable diagnostic or recovery disks, firmware upgrades, or a new image from the hard drive, CD/DVD/Blu-ray, USB, or the network.

### Enabling and disabling devices
Typical systems are loaded with onboard ports and features. The BIOS provides the ability to enable, disable, and configure devices, such as network adapters, storage, audio, and video ports, etc...

You can configure SATA configuraiton options to enable ot disable SATA and eSATA ports, and to configure SATA host adapters to run in compatible (emulating PATA), native (AHCI), or RAID modes. AHCI supports Native Command Queueing (NCQ) for faster performance and permits hot-swapping eSATA drives.

You can enable or disable USB 2.0 USB 3.0 (SuperSpeed). If you do not enable these version the system's USB ports will run at the next lower speed. You can also configure a USB port to output at a higher amperage to enable faster charging of smartphones.

YOu can also configure the integrated ports, such as audio and ethernet ports.

### Date/time
Provides ability to view and change the system data and time settings.

### Clock speeds
Though automatically detected on most recent systems, you can overclock the system by manually setting the frequency, CAS latency, etc... to enable memory of different speeds to be used safely by lowering settings. This can cause instability in the computer.

Some systems will default to lower values if the system doesn't start properly. You can customize the speeds, voltages, and other timings to provide more targeted performance.

### Virtualization support
When enabled, runs hardware-based virtualization programs (I.E. Hyper-V, Parallels), so the OS can run multiple operating systems. Though virtualization doesn't require processor support, virtualization programs perform much better on systems with hardware-assisted virtualization support enabled. In order to support hardware-assisted virtualization, the system must have a CPU that supports virtualization and virtualization must be enable in the BIOS.

Intel processor that include VT-x technology and AMD processors that include AMD-V tehcnology support hardware-assisted virtualization.

Intel systems with VT support may have 2 entries for virtualization. Intel VT (Virtualization Technology) must be enabled for hardware-assisted virtualization. Intel VT with Directed I/O (VT-d Tech) can also be enabled to improve I/O performance. Processors that support VT-x vary in level of VT-d support. Some systems have a single entry that enables virtualization. When VT-d is enabled, VT-x is also.

AMD systems that support hardware-assisted virtualization have a single BIOS setting that may be labeled Virtualization, Secure Virtual Mode, or SVM.

### BIOS security (passwords, drive encryption: TPM, LoJack, secure boot)
With a BIOS password enabled, you can permit access to the BIOS setup to only those with the password. With a power-on password, you can prevent anyone without the password from starting the system.

The BIOS password can be referred to as the Supervisor password. and the Power-on password can be called the User or BIOS password.

> All BIOS security password features can be bypassed by simply clearing the CMOS memory.

On older systems boot sector protection protected the default systems drive's boto sector from being changed by virus or other unwanted programs. On some this would have to be disabled to image the system.

Secure boot is a part of the UEFI specification permits only software trusted by the PC manufacturer to be used to boot the system. When enabled the UEFI checks for signatures on the software, options ROMs, and the OS. Software will not run without the proper signature. First introduced in Windows 8, RT, and Server 2012, it is also supported and sometimes required in newer versions. It is also supported in Linux Fedora, openSUSE, and Ubuntu. 

Trusted PLatform Module (TPM) is used by Windows OS editions that support BitLocker full-disk encryption to protect the content of the system hard drive (Vista) or any specified drive (7/8/8.1/10). Many corporate laptops include a built-in TPM, but computers and servers may include a connection for an optional TPM. The TPM adds advanced cryptogrpahics functions to the system.

LowJack for mobile devices is an embedded security feature that contains a BIOS-resident component and the Computrace agent, which is activated when a computer is reported as stolen. It can be added to other systems. LoJack is licensed, but COmpuTrace is the acutal agent. It is built into the OS and re-installs itself if removed or a new storage drive is installed. It has a phone home function, which can provide location information, and a theft mode, which will remotely lock the laptop and/or delete files and enforces a startup password.

## Built-in diagnostics
POST performs system checks at boot to ensure that everything is okay. It also emits beeps which provide information about the system via beep code, which you can check with the manufacturer beep codes to "translate". This helps as we become used to the "normal" beep code, that when an unrecognized beep code will grab our attention to possible problems.

Although OS include power management features, the BIOS controls how the system responds to standby or power-out conditions. ACPI is the power management function in modern systems, replacing the old APM standard, and it should be enabled. Most systems offer 2 ACPI states: S1/POS (power on standby) and S3/STR (suspend to RAM). You should use S3/STR when possible as it uses much less power when the system is idle. You can also configure the system power button, to to restart when AC power is removeds, and how to wake up from standby, sleep, or hibernation modes.

## Monitoring
Provides monitoring of the system. The Hardware MOnitor BIOS, or PC Health, is a common feature in newer systems.

### Temperature monitoring
Provides information on the temperature of the system, providing information to possible cooling issues. You can also configure the BIOS to warn when CPU or system temperatures reach high levels or if a fan fails or spins too slowly.

### Fan speeds
Provides information on fan speed and status, which with temperature monitoring, can point to fan issues.

### Intrusion detection/notification
Also known as Chassis intrusion, when enabled displays a warning on startup that the system has been opened.

### Voltage
Provides information on voltages running through the devices.

### Clock
Provides information on the clock.

### Bus speed
Provides information on bus speeds, or how fast data can travel within the circuitry on the motherboard.
